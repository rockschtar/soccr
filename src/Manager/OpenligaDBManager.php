<?php

namespace Rockschtar\Soccr\Manager;

use Andinger\OpenLigaDbApi\Client;
use Rockschtar\Soccr\Models\Leagues;
use Rockschtar\Soccr\Models\Team;
use Rockschtar\Soccr\Models\Teams;

class OpenligaDBManager {


    /**
     * @var Client
     */
    private $client;

    /**
     * OpenligaDBManager constructor.
     * @throws \Exception
     */
    public function __construct() {
        $this->client = new Client();
    }

    /**
     * @param string $leagueSortcut
     * @param string $season
     * @return Teams
     * @throws \SoapFault
     */
    public function getAvailableTeams(string $leagueSortcut, string $season) : Teams {

        $openligaDBTeams = $this->client->getTeamsByLeagueSeason($leagueSortcut, $season);
        $teams = new Teams();

        foreach($openligaDBTeams as $openligaDBTeam) {
            $teams->append(new Team($openligaDBTeam->getId(), $openligaDBTeam->getName(), $openligaDBTeam->getIconURL()));
        }

        return $teams;
    }

    /**
     * @return Leagues
     * @throws \SoapFault
     */
    public function getAvailableLeagues() : Leagues {
        $openligaDBLeagues = $this->client->getAvailableLeaguesBySport(1);

        $leagues = new Leagues();

        foreach($openligaDBLeagues as $openligaDBLeague) {
            $leagues->append($openligaDBLeague);
        }

        return $leagues;

    }

    public static function getNextMatchByTeam(int $teamId, string $leagueShortcut) : void {

            $openliga_db = new OpenLigaDB();
            $currentDate = new DateTime();
            $fromDate = clone $currentDate;
            $toDate = clone $currentDate;
            $fromDate->sub(new DateInterval('PT3H'));
            $toDate->add(new DateInterval('P60D'));

            $matches = $openliga_db->GetMatchdataByLeagueDateTime($leagueShortcut. $fromDate, $toDate);




    }

}