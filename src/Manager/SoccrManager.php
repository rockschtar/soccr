<?php
/**
 * Created by PhpStorm.
 * User: Stefan Helmer
 * Date: 03.03.2019
 * Time: 14:13
 */

namespace Rockschtar\Soccr\Manager;


use Andinger\OpenLigaDbApi\Api\League;

class SoccrManager {

    public static function getLeagueShortcuts() : array {
        $defaultLeagues = ['bl1' => '1. Bundesliga', 'bl2' => '2. Bundesliga', 'bl3' => '3. Bundesliga'];
        return apply_filters('soccr_league_shortcuts', $defaultLeagues);
    }

}