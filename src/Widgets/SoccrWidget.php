<?php

namespace Rockschtar\Soccr\Widgets;

use Rockschtar\Soccr\Manager\SoccrManager;

class SoccrWidget extends \WP_Widget {

    public function __construct() {
        parent::__construct(
            'soccr', __('Soccr Widget', 'soccr'), array('description' => __('Displays last or next match from a specified team', 'soccr'),)
        );
    }


    public function form($instances) : void {
        $currentYear = date('Y');
        $instance_league_shortcut = $instance['leagueShortcut'] ?? 'bl1';
        $instance_season = $instance['season'] ?? $currentYear - 1;
        $instance_title = $instance['title'] ?? __('Next Match', 'soccr');
        $instance_widgettype = $instance['widgettype'] ?? 'next';
        $instance_team = $instance['team'] ?? -1;

        $leagueShortcuts = SoccrManager::getLeagueShortcuts();

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'soccr'); ?>:</label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $instance_title; ?>" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('widgettype'); ?>"><?php _e('Mode', 'soccr'); ?>:</label>
            <select class="widefat" id="<?php echo $this->get_field_id('widgettype'); ?>" name="<?php echo $this->get_field_name('widgettype'); ?>">
                <option value="next" <?php selected($instance_widgettype, 'next'); ?>><?php _e('Next Match', 'soccr'); ?></option>
                <option value="last" <?php selected($instance_widgettype, 'last'); ?>><?php _e('Last Match', 'soccr'); ?></option>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('leagueShortcut'); ?>"><?php _e('League', 'soccr'); ?>:</label>
            <select class="widefat soccr-season-option" data-widgetid="<?php echo $this->id; ?>" id="<?php echo $this->get_field_id('leagueShortcut'); ?>" name="<?php echo $this->get_field_name('leagueShortcut'); ?>">
                <?php foreach($leagueShortcuts as $leagueShortcut => $leagueName): ?>
                    <option value="<?php echo $leagueShortcut ?>"><?php echo $leagueName; ?></option>
                <?php endforeach; ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('season'); ?>"><?php _e('Season', 'soccr'); ?>:</label>
            <select class="widefat soccr-season-option" data-widgetid="<?php echo $this->id; ?>" id="<?php echo $this->get_field_id('season'); ?>" name="<?php echo $this->get_field_name('season'); ?>">
                <?php for ($s = $currentYear - 1; $s <= $currentYear; $s++): ?>
                    <option value="<?php echo $s; ?>" <?php selected($instance_season, $s) ?>><?php echo $s; ?>/<?php echo $s+1; ?></option>
                <?php endfor; ?>
            </select>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('team'); ?>"><?php _e('Team', 'soccr'); ?>:</label>
            <select class="widefat" id="<?php echo $this->get_field_id('team'); ?>" name="<?php echo $this->get_field_name('team'); ?>">

            </select>
        </p>
        <?php


    }

}