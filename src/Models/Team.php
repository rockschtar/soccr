<?php
/**
 * Created by PhpStorm.
 * User: Stefan Helmer
 * Date: 22.02.2019
 * Time: 18:34
 */

namespace Rockschtar\Soccr\Models;


class Team implements \JsonSerializable {

    /**
     * @var int
     */
    private $teamId;

    /**
     * @var string
     */
    private $teamName;

    /**
     * @var string
     */
    private $iconUrl;

    /**
     * Team constructor.
     * @param int $teamId
     * @param string $teamName
     * @param string $iconUrl
     */
    public function __construct(int $teamId, string $teamName, string $iconUrl) {
        $this->teamId = $teamId;
        $this->teamName = $teamName;
        $this->iconUrl = $iconUrl;
    }

    /**
     * @return int
     */
    public function getTeamId(): int {
        return $this->teamId;
    }

    /**
     * @param int $teamId
     * @return Team
     */
    public function setTeamId(int $teamId): Team {
        $this->teamId = $teamId;
        return $this;
    }

    /**
     * @return string
     */
    public function getTeamName(): string {
        return $this->teamName;
    }

    /**
     * @param string $teamName
     * @return Team
     */
    public function setTeamName(string $teamName): Team {
        $this->teamName = $teamName;
        return $this;
    }

    /**
     * @return string
     */
    public function getIconUrl(): string {
        return $this->iconUrl;
    }

    /**
     * @param string $iconUrl
     * @return Team
     */
    public function setIconUrl(string $iconUrl): Team {
        $this->iconUrl = $iconUrl;
        return $this;
    }

    public function jsonSerialize() : array {

        return ['team_id' => $this->getTeamId(),
                'name' => $this->getTeamName(),
                'icon_url' => $this->getIconUrl()
        ];

    }


}