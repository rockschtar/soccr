<?php

namespace Rockschtar\Soccr\Models;

use Rockschtar\TypedArrays\TypedArray;

class Teams extends TypedArray {

    public function current() : Team {
        return parent::current();
    }

    public function getType(): string {
        return Team::class;
    }

    protected function isDuplicate($value): bool {
        return false;
    }
}