<?php
/**
 * Created by PhpStorm.
 * User: Stefan Helmer
 * Date: 22.02.2019
 * Time: 20:40
 */

namespace Rockschtar\Soccr\Models;


use Andinger\OpenLigaDbApi\Api\League;
use Rockschtar\TypedArrays\TypedArray;

class Leagues extends TypedArray {
    public function current(): League {
        return parent::current();
    }

    public function getType(): string {
        return League::class;
    }

    protected function isDuplicate($value): bool {
        return false;
    }
}