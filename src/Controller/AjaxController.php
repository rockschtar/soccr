<?php
/**
 * Created by PhpStorm.
 * User: Stefan Helmer
 * Date: 03.03.2019
 * Time: 14:23
 */

namespace Rockschtar\Soccr\Controller;


use Rockschtar\Soccr\Manager\OpenligaDBManager;
use Rockschtar\Soccr\Manager\SoccrManager;

class AjaxController {

    private function __construct() {
        add_action('wp_ajax_soccr_get_available_teams', array(&$this, 'get_available_teams'));

    }

    public static function &init() {
        static $instance = false;
        if (!$instance) {
            $instance = new self();
        }
        return $instance;
    }

    public function get_available_teams() : void {

        $league_shortcut = filter_input(INPUT_GET, 'league_shortcut', FILTER_SANITIZE_STRIPPED);
        $season = filter_input(INPUT_GET, 'season', FILTER_SANITIZE_STRIPPED);

        try {
            $openliga_db_manager = new OpenligaDBManager();
            $teams = $openliga_db_manager->getAvailableTeams($league_shortcut, $season);
            wp_send_json_success($teams);
        } catch (\Exception $ex) {
            wp_send_json_error($ex->getMessage(), 500);
        }

        wp_die();
    }
}