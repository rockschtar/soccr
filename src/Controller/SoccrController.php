<?php

namespace Rockschtar\Soccr\Controller;

use Rockschtar\Soccr\Manager\OpenligaDBManager;
use Rockschtar\Soccr\Widgets\SoccrWidget;

class SoccrController {

    private function __construct() {
        add_action('init', array(&$this, 'load_plugin_textdomain'));
        add_action('widgets_init', array(&$this, 'register_widgets'));
        add_action('admin_print_scripts-widgets.php', array(&$this, 'admin_print_scripts_widgets'), 20);

        AjaxController::init();

    }

    public static function &init() {
        static $instance = false;
        if (!$instance) {
            $instance = new self();
        }
        return $instance;
    }

    public function load_plugin_textdomain(): void {
        load_plugin_textdomain('soccr', true, SOCCR_PLUGIN_RELATIVE_DIR . '/languages/');
    }

    public function register_widgets(): void {
        register_widget(SoccrWidget::class);
    }

    public function admin_print_scripts_widgets(): void {
        wp_enqueue_script('soccr-widget-form', SOCCR_PLUGIN_URL . 'js/SoccrWidgetForm.js', array('wp-i18n', 'jquery'));
        wp_set_script_translations('soccr-widget-form', 'soccr', SOCCR_PLUGIN_DIR . DIRECTORY_SEPARATOR . 'languages');

        // wp_localize_script('soccr-widget-forms', "soccr_widget", array("text" => array("teams_loading" => __("Loading...Please wait"), "no_teams_available" => __("No Teams available", "soccr"))));

    }

}