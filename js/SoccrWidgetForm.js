
let SoccrWidgetForm = (function () {

    const { __, _x, _n, _nx } = wp.i18n;

    let teamSelectInput;
    let teamSelectOptionLoading = '<option value="-1">' + __('Lade Mannschaften', 'soccr') + '</option>';
    let teamSelectOptionEmpty = '<option value="-1">' + __('Keine Mannschaften verfügbar', 'soccr') + '</option>';

    function init() {

        $('.soccr-season-option').on('change', function () {

            let widgetId = $(this).data("widgetid");
            let leagueShortcut = $('#widget-' + widgetId + "-leagueShortcut").val();
            let season = $('#widget-' + widgetId + "-season").val();
            teamSelectInput = $('#widget-' + widgetId + "-team");

            loadTeams(leagueShortcut, season);
        });
    }

    function loadTeams(leagueShortcut, season) {

        teamSelectInput.html(teamSelectOptionLoading);
        teamSelectInput.prop('disabled', true);

        $.ajax({
            url: ajaxurl,
            type: 'GET',
            data: {
                action: 'soccr_get_available_teams',
                league_shortcut: leagueShortcut,
                season: season
            },
            success: function (response) {

                let teams = response.data;

                if (teams.length > 0) {
                    teamSelectInput.html(teamSelectOptionEmpty);
                    return;
                }

                let options = '';

                for (let team in teams) {
                    options += '<option value="' + teams[team].team_id + '">' + teams[team].name + '</option>';
                }

                teamSelectInput.prop('disabled', false);
                teamSelectInput.show();
                teamSelectInput.html(options);
            },
            fail: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            done: function (response) {

            }
        });
    }

    return {
        init: init
    }
})();

jQuery(function () {
    SoccrWidgetForm.init();
});

